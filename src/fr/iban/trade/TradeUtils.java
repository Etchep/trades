package fr.iban.trade;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.trade.data.TradeData;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class TradeUtils {

	private final Main main = JavaPlugin.getPlugin(Main.class);

	private int leftEmptySlots[] = {0,1,2,3,9,10,11,12,18,19,20,21,27,28,29,30,36,37,38,39};

	public enum InputType {
		FACTION_POINTS, MONEY, SHOP_POINTS, NOT_INPUTTING
	}

	public boolean hasRequestFrom(Player requested, Player requester) {
		if(main.tradeReq.containsKey(requester.getUniqueId())){
			if(main.tradeReq.get(requester.getUniqueId()).equals(requested.getUniqueId())) return true;
		}
		return false;
	}

	public void sendRequest(Player sender, Player receiver) {
		main.tradeReq.put(sender.getUniqueId(), receiver.getUniqueId());
		receiver.sendMessage("�m--�7�m----�f �6[�eCW�6] �a " + sender.getName() + " �esouhaite faire un �change �7�m----�f�m--");
		String s = "                       �6�l>> �e�lCLIC ICI POUR ACCEPTER �6�l<<";
		TextComponent ss = new TextComponent(s);
		ss.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�9Clic pour d�buter l'�change").create()));
		ss.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/trade accept " + sender.getName()));
		receiver.spigot().sendMessage(ss);
		sender.sendMessage("�aVotre demande d'�change a bien �t� envoy�e.");
	}
	
	public void removeRequest(Player sender) {
		main.tradeReq.remove(sender.getUniqueId());
	}

	public boolean isTrading(Player p) {
		return main.tradeMap.containsKey(p.getUniqueId());
	}

	public TradeData getTradeData(Player p) {
		return main.tradeMap.get(p.getUniqueId());
	}

	public void startTrading(Player p1, Player p2) {
		main.tradeMap.put(p1.getUniqueId(), new TradeData(p1, p2));
		main.tradeMap.put(p2.getUniqueId(), new TradeData(p2, p1));
	}

	public Player getTradePartner(Player p) {
		return getTradeData(p).getAdversaire();
	}

	public void stopTrading(Player p1, Player p2) {
		p2.sendMessage("�cVotre partenaire a mis fin � l'�change.");
		p1.sendMessage("�cVous avez mis fin � l'�change.");
		getTradeData(p1).getItemsBlack();
		getTradeData(p2).getItemsBlack();
		main.tradeMap.remove(p1.getUniqueId());
		main.tradeMap.remove(p2.getUniqueId());
		p2.closeInventory();
	}

	public boolean isInputting(Player p) {
		return getTradeData(p).getInputType() != InputType.NOT_INPUTTING;
	}

	public void setInputting(Player p, InputType type) {
		getTradeData(p).setInputType(type);
		if(type != InputType.NOT_INPUTTING)
		p.closeInventory();
	}

	public boolean setInput(Player p, InputType type, int input) {
		TradeData data = getTradeData(p);
		switch (type) {
		case FACTION_POINTS:
			if(main.playerData.get(p.getUniqueId()).getPoints() >= input) {
				data.setFaction_points(input);
				return true;
			}else {
				p.sendMessage("�cVous n'avez pas suffisament de points !");
				return false;
			}
		case MONEY:
			if(main.getEcon().getBalance(p) >= input) {
				data.setMoney(input);
				return true;
			}else {
				p.sendMessage("�cVous n'avez pas suffisament d'argent !");
				return false;
			}
		case SHOP_POINTS:
			data.setShop_points(input);
			return true;
		default:
			p.sendMessage("�cUne erreur est survenue, veuillez contacter un administrateur.");
			return false;
		}
	}

	public void updateTradeInventory(Player p) {
		getTradeData(p).updateTradeInventory(getTradeData(p), getTradeData(getTradeData(p).getAdversaire()));
	}

	public void openTradeInventory(Player p) {
		getTradeData(p).openTradeInventory(p);
	}
	
	public Inventory getTradeInventory(Player p) {
		return getTradeData(p).getInv();
	}

	public boolean leftEmptySlotContain(int slot) {
		int i = 0;
		for(int lep : leftEmptySlots) {
			if(lep == slot) {
				i++;
			}
		}
		return i != 0;
	}

	public void startTradeCountdown(Player p) {
		getTradeData(p).startCountDown(main, getTradeData(p), getTradeData(getTradePartner(p)));
	}

	public void setTimerCancelled(Player p) {
		updateTradeInventory(p);
		getTradeData(p).setTimerCancelled(true);
	}

}
