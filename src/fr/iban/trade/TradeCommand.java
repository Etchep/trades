package fr.iban.trade;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TradeCommand implements CommandExecutor{
	
	TradeUtils utils = new TradeUtils();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(!(sender instanceof Player)) return false;
		Player p = (Player)sender;
		if(args.length == 0){
			p.sendMessage(new String[]{
					"�7/Trade <joueur> �8Envoyer une demande d'�change.",
					"�7/Trade remove <joueur> �8Retirer une demande d'�change.",
					"�7/Trade accept <joueur> �8Accepter la demande d'�change.",
			});
		}else{
			switch(args[0].toLowerCase()){
			case "accept":
				if(args.length == 2) {
					Player requester = Bukkit.getPlayer(args[1]);
					if(requester != null) {
						if(utils.hasRequestFrom(p, requester)) {
							utils.startTrading(p, requester);
							utils.removeRequest(requester);
						}else {
							p.sendMessage("�cVous n'avez pas de demande de ce joueur.");
						}
					}else {
						p.sendMessage("�cLe joueur " + args[1] + " n'est pas en ligne.");
					}
				}else {
					p.sendMessage("�7/Trade accept <joueur> �8Accepter la demande d'�change");
				}
				break;
			case "remove":
				if(args.length == 2) {
					Player requested = Bukkit.getPlayer(args[1]);
					if(requested != null) {
						if(utils.hasRequestFrom(p, requested)) {
							utils.removeRequest(p);
							p.sendMessage("�aVotre demande d'�change a bien �t� r�tir�e.");
							requested.sendMessage("�c" + p.getName() + " a retir� sa demande d'�change.");
						}
					}else {
						p.sendMessage("�cLe joueur " + args[1] + " n'est pas en ligne.");
					}
				}else {
					p.sendMessage("�7/Trade remove <joueur> �8Retirer la demande d'�change envoy�e.");
				}
				break;
			default :
				Player target = Bukkit.getPlayer(args[0]);
				if(target != null) {
					if(!target.equals(p)) {
						utils.sendRequest(p, target);
					}else {
						p.sendMessage("�cVous ne pouvez pas vous envoyer une demande � vous m�me !");
					}
				}else {
					p.sendMessage("�cLe joueur " + args[0] + " n'est pas en ligne.");
				}
				break;
			}
		}
		
		return false;

	}

}
