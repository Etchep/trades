package fr.iban.trade;

import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import fr.iban.fclassement.PlayerData;
import fr.iban.trade.data.TradeData;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {
	
	public HashMap<UUID, TradeData> tradeMap;
	public HashMap<UUID, UUID> tradeReq;
	public HashMap<UUID, PlayerData> playerData;
	
	public void onEnable() {
		super.onEnable();
		Bukkit.getServer().getPluginManager().registerEvents(new Listeners(), this);
		getCommand("trade").setExecutor(new TradeCommand());
		tradeMap = new HashMap<UUID, TradeData>();
		tradeReq = new HashMap<UUID, UUID>();
		playerData = fr.iban.fclassement.Main.PlayerMap;
		setupEconomy();
		if (!setupEconomy()) {
			getLogger().severe(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
	}

	public void onDisable() {
		super.onDisable();
	}
	
	public static Economy econ = null;
	
	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public Economy getEcon() {
		return econ;
	}
}
