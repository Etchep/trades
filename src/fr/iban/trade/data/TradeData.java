package fr.iban.trade.data;

import org.bukkit.entity.Player;
import fr.iban.trade.TradeInventory;
import fr.iban.trade.TradeUtils.InputType;

public class TradeData extends TradeInventory{
	
	private Player adversaire;
	private int shop_points = 0;
	private int faction_points = 0;
	private int money = 0;
	private InputType inputType = InputType.NOT_INPUTTING;
	private boolean isReady = false;

	
	public TradeData(Player p, Player adversaire) {
		super(p, adversaire);
		this.adversaire = adversaire;
	}

	public Player getAdversaire() {
		return adversaire;
	}

	public int getShop_points() {
		return shop_points;
	}

	public void setShop_points(int shop_points) {
		this.shop_points = shop_points;
	}

	public int getFaction_points() {
		return faction_points;
	}

	public void setFaction_points(int faction_points) {
		this.faction_points = faction_points;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public InputType getInputType() {
		return inputType;
	}

	public void setInputType(InputType inputType) {
		this.inputType = inputType;
	}

	public boolean isReady() {
		return isReady;
	}

	public void setReady(boolean isReady) {
		this.isReady = isReady;
	}

}
