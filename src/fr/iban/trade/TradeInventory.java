package fr.iban.trade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import fr.iban.trade.data.TradeData;

public class TradeInventory {

	private Inventory inv;
	private Player p;
	private boolean timerStarted = false;
	private boolean isTimerCancelled = false;

	private int leftEmptySlots[] = {0,1,2,3,9,10,11,12,18,19,20,21,27,28,29,30,36,37,38,39};
	private int rightEmptySlots[] = {5,6,7,8,14,15,16,17,23,24,25,26,32,33,34,35,41,42,43,44};

	public TradeInventory(Player p, Player adverse) {
		this.p = p;
		this.inv = setUpTradeInventory(adverse);
		p.openInventory(inv);
	}

	public ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}

	public ItemStack playerInfoFish(Player adverse){

		FPlayer fp = FPlayers.getInstance().getByPlayer(adverse);
		Faction faction = fp.getFaction();

		FPlayer fp2 = FPlayers.getInstance().getByPlayer(p);
		Faction faction2 = fp2.getFaction();

		ItemStack it = new ItemStack(Material.RAW_FISH, 1 , (short) 2);
		ItemMeta im = it.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("�6�l" + adverse.getName());
		description.add("�eFaction : �f�m-�7�m--�f " +  faction.getRelationTo(faction2).getColor() + faction.getTag() + " �7�m--�f�m-");
		description.add("�eGrade faction :  �f�m-�7�m--�f " + faction.getRelationTo(faction2).getColor() + fp.getRole() + " �7�m--�f�m-");
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		im.setLore(description);
		it.setItemMeta(im);
		return it;
	}

	public Inventory setUpTradeInventory(Player adverse) {
		inv = Bukkit.createInventory(p, 54, "�7�lVOUS                     LUI");
		inv.setItem(4, playerInfoFish(adverse));
		//Ligne de s�paration : 4, 13, 22 , 31, 40, 49
		inv.setItem(13, getItem(Material.STAINED_GLASS_PANE, "", 1, 0, "", false));
		inv.setItem(22, getItem(Material.VINE, "", 1, 0, "", false));
		inv.setItem(31, getItem(Material.VINE, "", 1, 0, "", false));
		inv.setItem(40, getItem(Material.STAINED_GLASS_PANE, "", 1, 0, "", false));
		inv.setItem(49, getItem(Material.STAINED_GLASS_PANE, "", 1, 0, "", false));

		//ConfirmItem : 48 et 50
		inv.setItem(48, getItem(Material.COOKED_FISH, "�4�lPas Pr�t", 1, 0, "�aCliquez ici quand vous �tes pr�t", false));
		inv.setItem(50, getItem(Material.COOKED_FISH, "�4�lPas pr�t", 1, 0, "�cVotre partenaire n'est pas encore pr�t.", false));
		//Money items : 45 et 53
		inv.setItem(45, getItem(Material.GOLD_NUGGET, "�6Argent", 1, 0, "�eOffre actuelle :�6 0$", false));
		inv.setItem(53, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));
		// Fac points item : 46 et 52
		inv.setItem(46, getItem(Material.GLOWSTONE_DUST, "�6Points Faction", 1, 0, "�eOffre actuelle :�6 0", false));
		inv.setItem(52, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));
		// pb item : 47 et 51
		inv.setItem(47, getItem(Material.NETHER_STAR, "�6Points Boutique", 1, 0, "�eOffre actuelle :�6 0", false));
		inv.setItem(51, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));


		return inv;
	}

	public void updateTradeInventory(TradeData pData, TradeData adversaireData) {
		ArrayList<ItemStack> list = new ArrayList<ItemStack>();
		for(int i : leftEmptySlots) {
			if(adversaireData.getInv().getItem(i) != null && adversaireData.getInv().getItem(i).getType() != Material.AIR) {
				list.add(adversaireData.getInv().getItem(i));
			}
		}
		Iterator<ItemStack> itemIterator = list.iterator();
		for(int i : rightEmptySlots) {
			if(itemIterator.hasNext()) {
				inv.setItem(i, itemIterator.next());
			}
		}
		inv.setItem(45, getItem(Material.GOLD_NUGGET, "�6Argent", 1, 0, "�eOffre actuelle :�6" + pData.getMoney() + "$", false));
		inv.setItem(46, getItem(Material.GLOWSTONE_DUST, "�6Points Faction", 1, 0, "�eOffre actuelle :�6 " + pData.getFaction_points(), false));
		inv.setItem(47, getItem(Material.NETHER_STAR, "�6Points Boutique", 1, 0, "�eOffre actuelle :�6 " + pData.getShop_points(), false));

		if(pData.isReady()) {
			inv.setItem(48, getItem(Material.RAW_FISH, "�2�lPr�t", 1, 0, "�aCliquez pour ne plus �tre pr�t.", false));
		}else {
			inv.setItem(48, getItem(Material.COOKED_FISH, "�4�lPas Pr�t", 1, 0, "�aCliquez ici quand vous �tes pr�t", false));
		}
		if(adversaireData.isReady()) {
			inv.setItem(50, getItem(Material.RAW_FISH, "�2�lPr�t", 1, 0, "�aVotre adversaire est pr�t !.", false));
		}
		if(adversaireData.getFaction_points() != 0) {
			inv.setItem(52, getItem(Material.GLOWSTONE_DUST, "�6Points Faction", 1, 0, "�eOffre actuelle :�6 " + adversaireData.getFaction_points(), false));
		}else {
			inv.setItem(52, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));
		}
		if(adversaireData.getMoney() != 0) {
			inv.setItem(53, getItem(Material.GOLD_NUGGET, "�6Argent", 1, 0, "�eOffre actuelle :�6 "+ adversaireData.getMoney() + "$", false));
		}else {
			inv.setItem(53, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));
		}
		if(adversaireData.getShop_points() != 0) {
			inv.setItem(51, getItem(Material.NETHER_STAR, "�6Points Boutique", 1, 0, "�eOffre actuelle :�6 " + adversaireData.getShop_points(), false));
		}else {
			inv.setItem(51, getItem(Material.STAINED_GLASS_PANE, "", 1, 7, "", false));
		}

	}

	public void startCountDown(Main plugin, TradeData pData, TradeData adversaireData) {
		setTimerStarted(true);
		Bukkit.getScheduler().runTaskTimer(plugin, new Runnable() {
			int timer = 5;
			@Override
			public void run() {
				if(timer >= 1) {
					if(!isTimerCancelled) {
						inv.setItem(48, getItem(Material.RAW_FISH, "�2�lPr�t", timer, 0, "�aCliquez pour annuler", false));
						inv.setItem(50, getItem(Material.RAW_FISH, "�2�lPr�t", timer, 0, "�aVotre adversaire est pr�t !.", false));
						timer--;
					}
				}else {
					if(timer == 0) {
						for(int i : leftEmptySlots) {
							if(adversaireData.getInv().getItem(i) != null && adversaireData.getInv().getItem(i).getType() != Material.AIR) {
								if(!isInventoryFull(p)) {
									p.getInventory().addItem(adversaireData.getInv().getItem(i));
								}else {
									p.getWorld().dropItem(p.getLocation(), adversaireData.getInv().getItem(i));
								}
							}
						}
						p.updateInventory();
						if(adversaireData.getMoney() != 0) {
							plugin.getEcon().depositPlayer(p, adversaireData.getMoney());
						}
						if(adversaireData.getFaction_points() != 0) {
							plugin.playerData.get(p.getUniqueId()).setPoints(plugin.playerData.get(p.getUniqueId()).getPoints() + adversaireData.getFaction_points());
						}
						if(adversaireData.getShop_points() != 0) {

						}
						if(pData.getMoney() != 0) {
							plugin.getEcon().withdrawPlayer(p, pData.getMoney());
						}
						if(pData.getFaction_points() != 0) {
							plugin.playerData.get(p.getUniqueId()).setPoints(plugin.playerData.get(p.getUniqueId()).getPoints() - pData.getFaction_points());
						}
						plugin.tradeMap.remove(p.getUniqueId());
						p.closeInventory();
						p.sendMessage("�aL'�change a �t� un succ�s !");
						timer = -1;
					}
				}
			}
		}, 0L, 20L);
	}

	public void getItemsBlack() {
		for(int i : leftEmptySlots) {
			if(inv.getItem(i) != null) {
				if(!isInventoryFull(p)) {
					p.getInventory().addItem(inv.getItem(i));
				}else {
					p.getWorld().dropItem(p.getLocation(), inv.getItem(i));
				}
			}
		}
		p.updateInventory();

	}

	public boolean isInventoryFull(Player p)
	{
		return p.getInventory().firstEmpty() == -1;
	}

	public void openTradeInventory(Player p) {
		p.openInventory(inv);
	}

	public Inventory getInv() {
		return inv;
	}

	public boolean isTimerStarted() {
		return timerStarted;
	}

	public void setTimerStarted(boolean timerStarted) {
		this.timerStarted = timerStarted;
	}

	public boolean isTimerCancelled() {
		return isTimerCancelled;
	}

	public void setTimerCancelled(boolean isTimerCancelled) {
		this.isTimerCancelled = isTimerCancelled;
	}

}
