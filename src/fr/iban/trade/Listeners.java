package fr.iban.trade;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.iban.trade.TradeUtils.InputType;

public class Listeners implements Listener {

	TradeUtils utils = new TradeUtils();

	@EventHandler
	public void onClick(final InventoryClickEvent e){
		if(!(e.getWhoClicked() instanceof Player)) return;
		Player p = (Player) e.getWhoClicked();
		
		if(p.getOpenInventory() == null) return;
		if(utils.isTrading(p)) {
			if(e.getClick().isShiftClick()) {
				e.setCancelled(true);
				return;
			}
			if(utils.getTradeInventory(p) == null) return;
			if(e.getClickedInventory().equals(utils.getTradeInventory(p))) {
				if(e.getInventory().getHolder() != p) return;
				if(utils.getTradeData(p).isTimerStarted()) {
					e.setCancelled(true);
					if(e.getSlot() == 48) {
						utils.setTimerCancelled(p);
						utils.setTimerCancelled(utils.getTradePartner(p));
						utils.stopTrading(p, utils.getTradePartner(p));
						p.closeInventory();
					}
				}else {
					if(utils.getTradeData(p).isReady() && e.getSlot() != 48) {
						e.setCancelled(true);
						return;
					}
					if(utils.leftEmptySlotContain(e.getSlot())) {
						e.setCancelled(false);
						return;
					}else {
						e.setCancelled(true);
						if(e.getCursor().getType() == Material.AIR || e.getCursor() == null) {
							switch (e.getSlot()) {
							case 48:
								//CONFIRM
								if(e.getCurrentItem().getType() == Material.COOKED_FISH) {
									if(utils.getTradeData(utils.getTradePartner(p)).isReady()) {
										utils.startTradeCountdown(p);
										utils.startTradeCountdown(utils.getTradePartner(p));
									}else {
										utils.getTradeData(p).setReady(true);
									}
								}else if(e.getCurrentItem().getType() == Material.RAW_FISH){
									utils.getTradeData(p).setReady(false);
								}
								break;
							case 45:
								utils.setInputting(p, InputType.MONEY);
								p.sendMessage("�a�lVeuillez �crire dans le chat le nombre d'argent que vous voulez �changer :");
								//MONEY
								break;
							case 46:
								utils.setInputting(p, InputType.FACTION_POINTS);
								p.sendMessage("�a�lVeuillez �crire dans le chat le nombre de points faction que vous voulez �changer :");
								//Faction points
								break;
							case 47:
								utils.setInputting(p, InputType.SHOP_POINTS);
								p.sendMessage("�a�lVeuillez �crire dans le chat le nombre de points boutique que vous voulez �changer :");
								//points boutique
								break;
							default:
								break;
							}
						}
					}
					utils.updateTradeInventory(p);
					utils.updateTradeInventory(utils.getTradePartner(p));
				}
			}
		}
	}
	
    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent e) {
        
        Player p = (Player)e.getWhoClicked();
        if (p.getOpenInventory() == null) return;
        
        if(!utils.isTrading(p)) return;

        if(utils.getTradeData(p).isReady()) return;
        
        for (int slot : e.getRawSlots()) {
        	if(!utils.leftEmptySlotContain(slot)) {
        		e.setCancelled(true);
        	}
        }
        
		utils.updateTradeInventory(p);
		utils.updateTradeInventory(utils.getTradePartner(p));
    }

	@EventHandler
	public void onClose(InventoryCloseEvent e){
		if(!(e.getPlayer() instanceof Player)) return;
		Player p = (Player) e.getPlayer();
		if(utils.isTrading(p)) {
			if(utils.isInputting(p) == false) {
				if(!utils.getTradeData(p).isTimerStarted()) {
					utils.stopTrading(p, utils.getTradePartner(p));
				}else {
					utils.setTimerCancelled(p);
					utils.setTimerCancelled(utils.getTradePartner(p));
					utils.stopTrading(p, utils.getTradePartner(p));
				}
			}
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		if(utils.isTrading(p)) {
			utils.stopTrading(p, utils.getTradePartner(p));
		}
	}
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		if(utils.isTrading(e.getPlayer())) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if(utils.isTrading(p)) {
			if(utils.isInputting(p)) {
				try{
					int input = Integer.parseInt(e.getMessage());
					if(input >= 0) {
						if(utils.setInput(p, utils.getTradeData(p).getInputType(), input) == true) {
							utils.updateTradeInventory(p);
							utils.updateTradeInventory(utils.getTradePartner(p));
							utils.openTradeInventory(p);
							utils.setInputting(p, InputType.NOT_INPUTTING);
						}else {
							p.sendMessage("�aR�essayez :");
						}
					}else {
						p.sendMessage("�aR�essayez :");
					}
				}catch (NumberFormatException ex) {
					p.sendMessage("�cVous n'avez pas entr� un nombre !");
					p.sendMessage("�aR�essayez :");
				}
				e.setCancelled(true);
			}
		}
	}
}
